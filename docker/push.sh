#!/bin/bash

. ./version.sh

export DOCKER_ID_USER="elfuego"
docker login
docker push docker.io/elfuego/$NAME:$VERSION
docker push docker.io/elfuego/$NAME:$VERSION_MA
docker push docker.io/elfuego/$NAME:latest
