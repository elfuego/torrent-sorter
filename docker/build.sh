#!/bin/bash

. ./version.sh

docker build --rm -t elfuego/$NAME ..
docker tag elfuego/$NAME elfuego/$NAME:$VERSION
docker tag elfuego/$NAME elfuego/$NAME:$VERSION_MA
docker tag elfuego/$NAME elfuego/$NAME:latest
docker tag elfuego/$NAME:$VERSION docker.io/elfuego/$NAME:$VERSION
docker tag elfuego/$NAME:$VERSION docker.io/elfuego/$NAME:$VERSION_MA
docker tag elfuego/$NAME:$VERSION docker.io/elfuego/$NAME:latest
