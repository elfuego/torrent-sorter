FROM python:3.9-alpine

RUN pip install --upgrade pip
RUN pip install qbittorrent-api==2022.8.38 bencoding==0.2.6

ADD torrent_sorter.py /

VOLUME /feed
VOLUME /data
WORKDIR /data

CMD [ "python", "-u", "/torrent_sorter.py" ]
