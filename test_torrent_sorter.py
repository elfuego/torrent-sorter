import os
import time
import unittest
from pathlib import Path

from pyfakefs import fake_filesystem_unittest

from torrent_sorter import get_hash, process, Conf, list_files, get_name, load_db, store_db, k_touch, k_tag, get_conf

TAG1 = "name1"

CAT1 = "неполные"

HASHES = ["5f38931478a122662eb4b061d5e8ee97b28fc351", "72296f13bc35a144feae9fd3586a34cb822138c2",
          "a9469dfab85289659e9b146239c2e7d292069a51"]

TAGS = [TAG1, "name2", "name3"]

DEF_SAVE_PATH = "/default"

FEED = '/torrents'
FEED2 = '/torrents2'


class MockTorrent:
    def __init__(self, parent, data, tag=None, category=None, save_path=None):
        self.parent = parent
        self.data = data

        self.hash = get_hash(data)
        self.name = get_name(data)

        self.tag = None
        if tag is not None:
            self.tag = tag
        self.category = None
        if category is not None:
            self.category = category
        self.save_path = DEF_SAVE_PATH
        if save_path is not None:
            self.save_path = save_path

    def add_tags(self, tags=None):
        self.tag = tags

    def delete(self):
        self.parent.delete(self)

    def key(self):
        return self.hash


class MockApi:

    def __init__(self):
        self.torrents = []

    def torrents_info(self, torrent_hashes=None, tag=None):
        for torrent in self.torrents:
            found = True
            if tag is not None and torrent.tag != tag:
                found = False
            if torrent_hashes is not None and torrent.hash != torrent_hashes:
                found = False
            if found:
                yield torrent

    def torrents_add(self, torrent_files=None, save_path=None, category=None, tags=None):
        torrent = MockTorrent(self, torrent_files, save_path=save_path, category=category, tag=tags)
        self.torrents.append(torrent)

    def delete(self, torrent):
        self.torrents.remove(torrent)

    @staticmethod
    def get_hash(torrent1):
        return torrent1.hash

    def first_for_tag(self, tag1):
        return next(self.torrents_info(tag=tag1), None)

    def first_for_hash(self, hash1):
        return next(self.torrents_info(torrent_hashes=hash1), None)

    def add(self, data1):
        self.torrents_add(torrent_files=data1)

    def replace(self, data1, torrent1):
        torrent1.delete()
        self.torrents_add(torrent_files=data1,
                          tags=torrent1.tag,
                          category=torrent1.category,
                          save_path=torrent1.save_path)


def parse_tags(tags):
    return [x.strip() for x in tags.split(',')]


def load_torrents():
    torrents = {}
    for f1 in list_files("torrents"):
        with open(f1.path, "rb") as fo1:
            torrents[Path(f1.path).name] = fo1.read()
    return torrents


def save_torrents(torrents, folder):
    os.makedirs(folder)
    for fn in torrents:
        with open(os.path.join(folder, fn), "wb") as fo1:
            fo1.write(torrents[fn])


class TestProcess(fake_filesystem_unittest.TestCase):
    def setUp(self):
        with open("1.torrent", "rb") as fo1:
            self.test_torrent1 = fo1.read()
        torrents = load_torrents()
        self.setUpPyfakefs()
        save_torrents(torrents, FEED)
        save_torrents({TAG1 + ".torrent": self.test_torrent1}, FEED2)
        self.conf = Conf()
        self.conf.feed = FEED
        self.client = MockApi()

    def test_empty(self):
        process(self.conf, self.client)

        cats = [None for _ in range(3)]
        paths = [DEF_SAVE_PATH for _ in range(3)]
        self.check_results(self.client, TAGS, HASHES, cats, paths)

    def test_replace(self):
        self.conf.feed = FEED2
        process(self.conf, self.client)

        list1 = list(self.client.torrents_info())
        self.assertIsNotNone(list1)
        self.assertEqual(1, len(list1))
        cat1_path = os.path.join(DEF_SAVE_PATH, CAT1)
        self.client.torrents[0].category = CAT1
        self.client.torrents[0].save_path = cat1_path

        self.conf.feed = FEED
        process(self.conf, self.client)

        cats = [CAT1, None, None]
        paths = [cat1_path, DEF_SAVE_PATH, DEF_SAVE_PATH]
        self.check_results(self.client, TAGS, HASHES, cats, paths)

    def test_existent(self):
        process(self.conf, self.client)
        process(self.conf, self.client)

        cats = [None for _ in range(3)]
        paths = [DEF_SAVE_PATH for _ in range(3)]
        self.check_results(self.client, TAGS, HASHES, cats, paths)

    def test_deleted(self):
        db = load_db()
        db[k_touch(TAG1)] = int(time.time())
        store_db(db)
        process(self.conf, self.client)

        cats = [None for _ in range(2)]
        paths = [DEF_SAVE_PATH for _ in range(2)]
        self.check_results(self.client, TAGS[1:], HASHES[1:], cats, paths)

    def test_found(self):
        with open(os.path.join(FEED, TAG1 + ".torrent"), "rb") as fo1:
            self.client.add(fo1.read())
        process(self.conf, self.client)

        cats = [None for _ in range(3)]
        paths = [DEF_SAVE_PATH for _ in range(3)]
        self.check_results(self.client, TAGS, HASHES, cats, paths)

    def test_tagged(self):
        with open(os.path.join(FEED, TAG1 + ".torrent"), "rb") as fo1:
            self.client.add(fo1.read())
        db = load_db()
        db[k_tag(TAG1)] = HASHES[0]
        store_db(db)
        process(self.conf, self.client)

        cats = [None for _ in range(3)]
        paths = [DEF_SAVE_PATH for _ in range(3)]
        self.check_results(self.client, TAGS, HASHES, cats, paths)

    def check_results(self, client, tags, hashes, cats, paths):
        list1 = list(client.torrents_info())
        self.assertIsNotNone(list1)
        self.assertEqual(len(hashes), len(list1))
        list1.sort(key=MockTorrent.key)
        i = 0
        for torrent in list1:
            # self.assertEqual(tags[i], torrent.tag)
            self.assertEqual(tags[i], torrent.name)
            self.assertEqual(hashes[i], torrent.hash)
            self.assertEqual(cats[i], torrent.category)
            self.assertEqual(paths[i], torrent.save_path)
            i += 1


class TestConf(unittest.TestCase):
    def test_empty(self):
        conf = get_conf()
        self.assertEqual("localhost", conf.host)
        self.assertEqual(8080, conf.port)
        self.assertEqual("admin", conf.user)
        self.assertEqual("adminadmin", conf.pwd)
        self.assertEqual("/feed", conf.feed)
        self.assertEqual(3600, conf.period)

    def test_full(self):
        os.environ["TS_HOST"] = "h"
        os.environ["TS_PORT"] = "80"
        os.environ["TS_USER"] = "u"
        os.environ["TS_PWD"] = "p"
        os.environ["TS_FEED"] = "/f"
        os.environ["TS_PERIOD_SEC"] = "60"
        conf = get_conf()
        self.assertEqual("h", conf.host)
        self.assertEqual(80, conf.port)
        self.assertEqual("u", conf.user)
        self.assertEqual("p", conf.pwd)
        self.assertEqual("/f", conf.feed)
        self.assertEqual(60, conf.period)

    def test_ints(self):
        os.environ["TS_PORT"] = "aa"
        conf = get_conf()
        self.assertEqual(8080, conf.port)
        os.environ["TS_PERIOD_SEC"] = "20"
        conf = get_conf()
        self.assertEqual(60, conf.period)
        os.environ["TS_PERIOD_SEC"] = "aa"
        conf = get_conf()
        self.assertEqual(3600, conf.period)


if __name__ == "__main__":
    unittest.main()
