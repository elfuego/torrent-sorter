import hashlib
import json
import os
import time
from pathlib import Path

import bencoding
import qbittorrentapi

MIN_PERIOD = 60
DEF_PERIOD = 3600

DB = 'torrent_sorter.json'


class Conf:
    host = None
    port = None
    user = None
    pwd = None
    feed = None
    period = None


def get_conf():
    conf = Conf()
    conf.host = os.environ.get("TS_HOST")
    conf.port = os.environ.get("TS_PORT")
    conf.user = os.environ.get("TS_USER")
    conf.pwd = os.environ.get("TS_PWD")
    conf.feed = os.environ.get("TS_FEED")
    conf.period = os.environ.get("TS_PERIOD_SEC")
    if conf.host is None:
        conf.host = "localhost"
    try:
        conf.port = int(conf.port)
    except:
        conf.port = 8080
    if conf.user is None:
        conf.user = "admin"
    if conf.pwd is None:
        conf.pwd = "adminadmin"
    if conf.feed is None:
        conf.feed = "/feed"
    try:
        conf.period = int(conf.period)
        if conf.period < MIN_PERIOD:
            conf.period = MIN_PERIOD
    except:
        conf.period = DEF_PERIOD
    return conf


def list_files(folder):
    for file1 in os.scandir(folder):
        if file1.is_file() and file1.path.endswith('.torrent'):
            yield file1


class QbtDecor:
    def __init__(self, client):
        self.client = client

    # noinspection PyMethodMayBeStatic
    def get_hash(self, torrent1):
        return torrent1.info.hash

    def first_for_tag(self, tag1):
        return next(iter(self.client.torrents_info(tag=tag1)), None)

    def first_for_hash(self, hash1):
        return next(iter(self.client.torrents_info(torrent_hashes=hash1)), None)

    def add(self, data1):
        self.client.torrents_add(torrent_files=data1)

    def replace(self, data1, torrent1):
        torrent1.delete(delete_files=False)
        self.client.torrents_add(torrent_files=data1,
                                 tags=torrent1.tags,
                                 category=torrent1.category,
                                 save_path=torrent1.save_path)


def connect_client(conf):
    qbt = qbittorrentapi.Client(host=conf.host, port=conf.port, username=conf.user, password=conf.pwd)
    qbt.auth_log_in()
    client = QbtDecor(qbt)
    return client


def get_torrent(file_name):
    with open(file_name, "rb") as objTorrentFile:
        data = objTorrentFile.read()
        hash1 = get_hash(data)
        tag1 = Path(objTorrentFile.name).stem
    return data, hash1, tag1


def get_hash(data):
    decoded_dict = bencoding.bdecode(data)
    return hashlib.sha1(bencoding.bencode(decoded_dict[b"info"])).hexdigest()


def get_name(data):
    decoded_dict = bencoding.bdecode(data)
    return decoded_dict[b"info"][b"name"].decode("utf-8")


def k_touch(tag1):
    return "touch:" + tag1


def k_tag(tag1):
    return "tag:" + tag1


def load_db():
    try:
        with open(DB, "r") as dbf:
            db = json.loads(dbf.read())
            if type(db) is not dict:
                db = {}
            return db
    except FileNotFoundError:
        return {}


def store_db(db):
    with open(DB, "w") as dbf:
        dbf.write(json.dumps(db))


def db_get(db, key):
    try:
        return db[key]
    except KeyError:
        return None


def process(conf, client):
    db = load_db()
    print(" >>> Started processing")
    for f1 in list_files(conf.feed):
        data1, hash1, tag1 = get_torrent(f1.path)
        print(f" > file: {Path(f1.path).name}, hash: {hash1}")
        tagged_hash = db_get(db, k_tag(tag1))
        if tagged_hash is not None:
            torrent1 = client.first_for_hash(tagged_hash)
            if torrent1 is not None:
                hash2 = client.get_hash(torrent1)
                if hash1 != hash2:
                    client.replace(data1, torrent1)
                    db[k_tag(tag1)] = hash1
                    db[k_touch(tag1)] = int(time.time())
                    print(f"+++found name match with different hash: {hash2}, replaced")
                    continue
                else:
                    if k_touch(tag1) not in db:
                        db[k_touch(tag1)] = int(time.time())
                    print("   found")
                    continue

        if k_touch(tag1) in db:
            print("   already processed")
            continue
        torrent2 = client.first_for_hash(hash1)
        db[k_tag(tag1)] = hash1
        db[k_touch(tag1)] = int(time.time())
        if torrent2:
            print("   found, tagged")
        else:
            client.add(data1)
            print("+++added new torrent")
    store_db(db)


def main():
    conf = get_conf()
    print(vars(conf))
    while True:
        client = connect_client(conf)
        process(conf, client)
        time.sleep(conf.period)


if __name__ == "__main__":
    main()
