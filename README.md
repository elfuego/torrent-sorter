### torrent sorter

For QBitTorrent (tested on v.4.4.5)

The script is intended to work on the feed folder of TorMon or another
tool which collects torrents and place them in a folder.
For incremental topics, when the topic maintainer adds new files to
the torrent and replaces it in the topic, it is important so that the
collector script generates the same file name for updated torrent files
(as TorMon does) for the `torrent sorter` to properly replace torrents,
recreating proper category, save path, and tags, set for the previous
torrent.


#### Configuration:

```yaml
services:
  tm:
    image: alfonder/torrentmonitor
...
    volumes:
      - /opt/tm/torrents:/data/htdocs/torrents
...
  tsorter:
    image: elfuego/torrent_sorter:latest
...
    environment:
      - TS_HOST=localhost
      - TS_PORT=8080
      - TS_USER=admin
      - TS_PWD=adminadmin
      - TS_PERIOD_SEC=3600
...
    volumes:
      - /opt/tm/torrents:/feed
...
```

Please, consider both volumes `/data/htdocs/torrents` for TorMon and
`/feed` for the `torrent sorter` point to the same folder.

For the environment variables the default values are shown.
